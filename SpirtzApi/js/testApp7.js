(function() {
	var spritzController = null;

	var onSpritzifySuccess = function(spritzText) {
		spritzController.startSpritzing(spritzText);
	};
	
	
	
	var onSpritzifyError = function(error) {
		alert("Unable to Spritz: " + error.message);
	};
	
    function clearTextClick() {
        $("#inputText").val("");
    }
    
    function fillTextClick() {
    	var testText = "Spritzing content from the web is also easy. All you need to do is publish content on the web, "
    		+ " and our platform can transform it into Spritz Text. The default configuration simply extracts all text " 
    		+ "from the HTML at the target URL, but advanced configuration allows finer control using CSS-like selectors.";    		
    	var text = $("#inputText").val();   	
        $("#inputText").val(text + (text != "" ? "\n" : "") + testText);
    }

	function onStartSpritzClick(event) {
		var text = $('#inputText').val();
		var locale = "de";
		
		// Send to SpritzEngine to translate
		SpritzClient.spritzify(text, locale, onSpritzifySuccess, onSpritzifyError);
		
	};
	
	function onParseSpritzClick(event) {
		var text = $('#inputText').val();
		
		sentences = text.split("\n");
		sentenceI = -1;
		
		parseNext();
	};
	
	var sentenceI = 0;
	var sentences = 0;
	var curSentence;
	
	
	function parseNext() {
		sentenceI++;
		if(sentences.length == sentenceI) {
			console.log("Parsing done");
			return;
		}
		else {
			
			console.log("Checking line: (" + sentenceI +" of "+sentences.length+") : "+ sentences[sentenceI]);
			curSentence = sentences[sentenceI].split(';');
			//if(curSentence.length > 1 && curSentence[1].charAt(0) == "0") {
			
			//	console.log("Parsing was skipped because signal 0 after ;");
			//	skipSentence();
			//} else {
				console.log("Ask API to parse sentence: "+sentences[sentenceI]);
				var locale = "de";
				SpritzClient.spritzify(curSentence[0], locale, onSpritzifyParseSuccess, onSpritzifyError);
			//}
		}
		
	}
	
	var skipSentence = function() {
		var text = $("#outputText").val();
		text += sentences[sentenceI] + "\n";
		$("#outputText").val(text);
		parseNext();
	}
	
	var onSpritzifyParseSuccess = function(spritzText) {
	
		console.log("Parsing by API was successful...");
		var text = "";
		var parsed = "";
		for(var i = 0; i < spritzText.words.length; i++) {
			var word = spritzText.words[i];
			parsed += word.word + "|" +word.flags +"|"+ word.multiplier + "|"+word.orp+"|"+word.position + "%";
		}
		 curSentence.unshift(parsed);
		console.log("Write down API result in format word.word + \"|\" +word.flags +\"|\"+ word.multiplier + \"|\"+word.orp+\"|\"+word.position + \"%\"");
		console.log(parsed);
		var text = $("#outputText").val();
		text += curSentence.join(';')+ "\n";
		$("#outputText").val(text);
		parseNext();
	};
	
	
	// Customized options
	var customOptions = {
            placeholderText:    { startText: '' },
			redicleWidth: 	    434,	// Specify Redicle width
			redicleHeight: 	    76		// Specify Redicle height
	};

	var init = function() {
		
		$("#clear").on("click", clearTextClick);
		$("#fill").on("click", fillTextClick);
		$("#startSpritz").on("click", onStartSpritzClick);	
		
		$("#parseSpritz").on("click", onParseSpritzClick);			
		 
 		// Construct a SpritzController passing the customization options
 		spritzController = new SPRITZ.spritzinc.SpritzerController(customOptions);
 		
 		// Attach the controller's container to this page's "spritzer" container
 		spritzController.attach($("#spritzer"));
	};
	
	
	$(document).ready(function() {
		init();
	});

	
})();


