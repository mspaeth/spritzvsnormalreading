function funs = presentation
    funs.spritzify = @spritzify;
    funs.drawScreenRect = @drawScreenRect;
    funs.drawContinuousText = @drawContinuousText;
    funs.drawQuestionmark = @drawQuestionmark;
    
    funs.showFixationSpritzbox = @showFixationSpritzbox;
    funs.presentTarget = @presentTarget;
    funs.showFixationSpritzboxReact = @showFixationSpritzboxReact;
    funs.drawSpritzWordReact = @drawSpritzWordReact;
    funs.showCrosshairReact = @showCrosshairReact;
    funs.drawSingleSpritzWord = @drawSingleSpritzWord;
end

function spritzify(window, centerx, centery, text, wpm)
    global mark

    
    spritzifiedText = spritzifyText(text);
    characterDimensions = getCharDimensions(window);
    
    n_words = length(spritzifiedText);
    
    framesPerSecond = 60;
    frameDuration = 1/framesPerSecond;
    animationDuration = .750;
    animationSteps = animationDuration / frameDuration;
    
    % 5: Spritzbox mit erstem Wort wird eingeblendet
    mark.sendMarker(5);
    for animationN = 1:animationSteps
        drawScreenRect(window, centerx, centery);
        drawAnimationRectangles(window, centerx, centery, characterDimensions, animationSteps,animationN);

        % DrawFormattedText(window, text,rect(1), rect(2), [0,0,0],1,0,0,0,0,rect)
        [pivotStartX, pivotStartY] = drawSpritzBox(window, centerx, centery, characterDimensions);

        drawSpritzWord(window, pivotStartX, pivotStartY, spritzifiedText(1));

        Screen('Flip', window);
        WaitSecs(frameDuration);
    end
    
    
    % 6: Spritzbox Animation beendet
    mark.sendMarker(6);
    
    for n = 1:n_words
        
        
        % if the parsed text contains a empty char, make shure to skip it
        if(length(spritzifiedText(n).word) == 0)
            continue;
        end
        drawScreenRect(window, centerx, centery);

        % DrawFormattedText(window, text,rect(1), rect(2), [0,0,0],1,0,0,0,0,rect)
        [pivotStartX, pivotStartY] = drawSpritzBox(window, centerx, centery, characterDimensions);

        
        drawSpritzWord(window, pivotStartX, pivotStartY, spritzifiedText(n));
        
        
        % 7: Spritzbox naechstes wort wird eingeblendet
        mark.sendMarker(7);
        
        Screen('Flip', window);
        
        spritzifiedText(n).multiplier;
        calculatedSecs = double( (1 + spritzifiedText(n).multiplier / 100) * floor(60000 /(wpm*1.21)) )/double(1000);
        WaitSecs(calculatedSecs);

        
    end
    
end


function sprizifiedText = spritzifyText(text)
    text
    % split words
    all_words = strsplit(text,'%');
    n_words = length(all_words);
    
    for i=1:n_words
        if(length(all_words{i}) == 0) 
            break;
        end    
        attributes = strsplit(all_words{i}, '|');
        spritzWords(i).word =attributes{1};
        spritzWords(i).flags =str2num(attributes{2});
        spritzWords(i).multiplier =str2num(attributes{3});
        spritzWords(i).opt = str2num(attributes{4});
        spritzWords(i).position =str2num(attributes{5});
      
    end
    
        fprintf('spritzified done');

    sprizifiedText = spritzWords;
end

function showFixationSpritzbox(window, centerX, centerY, time)
    global mark

    % 9: Anzeige der Fixations spritzbox 
    mark.sendMarker(9);
    
    characterDimensions = getCharDimensions(window);
    screenRect = drawScreenRect(window, centerX, centerY); 
    
    [pivotStartX, pivotStartY] = drawSpritzBox(window, centerX, centerY, characterDimensions);
    
    Screen('Flip', window);
    WaitSecs(time)
    
end


function drawQuestionmark(window, centerX, centerY, time)

    screenRect = drawScreenRect(window, centerX, centerY); 
    % mytext contains the content of the first 48 lines of the text file.
    % Let's print it: Start at (x,y)=(10,10), break lines after 40
    % characters:
    
    Screen('TextSize', window, 100);
    DrawFormattedText(window, '?', 'center', 'center', 0, 30, 0, 0, 2);
    Screen('TextSize', window, 20);
    Screen('Flip', window);
    WaitSecs(time)
end


function showCrosshairReact(window, centerX, centerY)
    global mark
    
    characterDimensions = getCharDimensions(window);
     % todo create xy coordinates used to draw the box
    maxChars = 17;
    characterWidth = characterDimensions(1);
    characterHeight = characterDimensions(2);
    
    boxWidth = characterWidth * maxChars;
    boxHeight = characterHeight * 1.3;
    
    pivotHeight = (boxHeight - characterHeight)/2;
    
    startY = centerY - (boxHeight / 2);
    startX = centerX - (boxWidth / 2);
    pivotStartX = startX + (boxWidth / 3);
    
   
    % 8: Anzeige des Fadenkreuz, auf das mit einem Tastendruck reagiert werden soll
    mark.sendMarker(8);
    characterDimensions = getCharDimensions(window);
    screenRect = drawScreenRect(window, centerX, centerY); 
    
    
    
    Screen('DrawLine', window, [0,0,0], pivotStartX-20, centerY,  pivotStartX+20, centerY, 3 );
    Screen('DrawLine', window, [0,0,0], pivotStartX, centerY-20,  pivotStartX, centerY+20, 3 );
    
    Screen('Flip', window);
    KbStrokeWait;
end

function drawSingleSpritzWord(window, centerX, centerY, word)
    characterDimensions = getCharDimensions(window);
    screenRect = drawScreenRect(window, centerX, centerY); 
    
    [pivotStartX, pivotStartY] = drawSpritzBox(window, centerX, centerY, characterDimensions);
    drawSpritzWord(window, pivotStartX, pivotStartY, createSpritzWord(word));
    
    Screen('Flip', window);
end

function response = drawSpritzWordReact(window, centerX, centerY, word)


    downKey = KbName('DownArrow');
    upKey = KbName('UpArrow');
    
    drawSingleSpritzWord(window, centerX, centerY, word)
    response = 0;
    while response == 0
        

        % Check the keyboard. The person should press the
        [keyIsDown,secs, keyCode] = KbCheck;
        if keyCode(downKey)
            response = 0;
            return
        elseif keyCode(upKey)
            response = 1;
            return
        end
        
    end
end

function presentTarget(window, centerX, centerY, text, time) 
    characterDimensions = getCharDimensions(window);

    screenRect = drawScreenRect(window, centerX, centerY); 
    
    [pivotStartX, pivotStartY] = drawSpritzBox(window, centerX, centerY, characterDimensions);
    drawSpritzWord(window, pivotStartX, pivotStartY, text);
    
    Screen('Flip', window);
    WaitSecs(time)
    
end 

function screenRect = drawContinuousText(window, centerX, centerY, text)

    screenRect = drawScreenRect(window, centerX, centerY); 
    % mytext contains the content of the first 48 lines of the text file.
    % Let's print it: Start at (x,y)=(10,10), break lines after 40
    % characters:
    DrawFormattedText(window, text, screenRect(1), 'center', 0, 30, 0, 0, 2);
end


function screenRect = drawScreenRect(window, centerX, centerY) 
    rectWidth = 430;
    rectHeight = 600;
    screenRect = [(centerX-(rectWidth/2)) (centerY-(rectHeight/2)) (centerX+(rectWidth/2)) (centerY+(rectHeight/2))];
    Screen('FillRect', window, [255 255 255], screenRect);
    
    screenRect2 = [(centerX-(rectWidth/2))-5 (centerY-(rectHeight/2))-5  (centerX+(rectWidth/2))+5 (centerY+(rectHeight/2))+5];
    Screen('FillRect', window, [255 255 255], screenRect2);
end

function [pivotStartX, pivotStartY] = drawSpritzBox(window, centerX, centerY, characterDimensions) 
    % todo create xy coordinates used to draw the box
    maxChars = 17;
    characterWidth = characterDimensions(1);
    characterHeight = characterDimensions(2);
    
    boxWidth = characterWidth * maxChars;
    boxHeight = characterHeight * 1.3;
    
    pivotHeight = (boxHeight - characterHeight)/2;
    
    startY = centerY - (boxHeight / 2);
    startX = centerX - (boxWidth / 2);
    pivotStartX = startX + (boxWidth / 3);
    
    pivotStartY = startY + pivotHeight;

    % draw top line
    Screen('DrawLine', window, [0,0,0], startX, startY, startX+boxWidth, startY );
    % draw top pivot marker
    Screen('DrawLine', window, [0,0,0], pivotStartX, startY, pivotStartX, startY+pivotHeight);
     
    % draw bottom line
    Screen('DrawLine', window, [0,0,0],  startX, startY+boxHeight, startX+boxWidth,startY+boxHeight);
    % draw bottom pivot marker
    Screen('DrawLine', window, [0,0,0],  pivotStartX, startY+boxHeight, pivotStartX, startY+boxHeight-pivotHeight);
     
end

function drawAnimationRectangles(window, centerX, centerY, characterDimensions, totalFrames, frame)  
     maxChars = 17;
    characterWidth = characterDimensions(1);
    characterHeight = characterDimensions(2);
    
    boxWidth = characterWidth * maxChars;
    boxHeight = characterHeight * 1.3;
    
    pivotHeight = (boxHeight - characterHeight)/2;
    
    startY = centerY - (boxHeight / 2);
    startX = centerX - (boxWidth / 2);
    pivotStartX = startX + (boxWidth / 3);
    
    pivotStartY = startY + pivotHeight;

    leftBoxTotal = pivotStartX - startX;
    rightBoxTotal = startX+boxWidth - pivotStartX;
    
    shrinkFactor = frame / totalFrames; 
    
    leftBoxTotalDelta = leftBoxTotal * shrinkFactor;
    rightBoxTotalDelta = rightBoxTotal * shrinkFactor;
    
    
    
    % draw top line
    Screen('FillRect', window, [0.7,0.7,0.7], [startX+leftBoxTotalDelta startY pivotStartX startY+boxHeight] );
    Screen('FillRect', window, [0.7,0.7,0.7], [pivotStartX startY startX+boxWidth-rightBoxTotalDelta startY+boxHeight] );
    % draw top pivot marker
   % Screen('DrawLine', window, [0,0,0], pivotStartX, startY, pivotStartX, startY+pivotHeight);
     
    % draw bottom line
    %Screen('DrawLine', window, [0,0,0],  startX, startY+boxHeight, startX+boxWidth,startY+boxHeight);
    % draw bottom pivot marker
    %Screen('DrawLine', window, [0,0,0],  pivotStartX, startY+boxHeight, pivotStartX, startY+boxHeight-pivotHeight);
end

function drawSpritzWord(window, centerx, centery, word) 
    wordLength = length(word.word);
    
    pivotLetter = word.opt+1;
    
    
    if wordLength > 1
        % calculate pivot position based on the word
        toPivotBox = Screen('TextBounds',window, word.word(1:(pivotLetter-1)));
        pivotBox = Screen('TextBounds',window, word.word(pivotLetter));

        startx = centerx - ((RectWidth(pivotBox)* 0.5 + RectWidth(toPivotBox)));
        
        % draw word till pivot
        [curX,curY] = Screen('DrawText', window, word.word(1:(pivotLetter-1)),startx, centery, [0,0,0]);

        % draw word pivot in red
        [curX,curY] = Screen('DrawText', window, word.word(pivotLetter), curX, curY, [1,0,0]);

        % draw word from pivot
        Screen('DrawText', window, word.word((pivotLetter+1):wordLength),curX, curY,[0,0,0]);
    else
        pivotBox = Screen('TextBounds',window, word.word(pivotLetter));

        startx = centerx - ((RectWidth(pivotBox)* 0.5));
        
        % draw word pivot in red
        Screen('DrawText', window, word.word(pivotLetter), startx, centery, [1,0,0]);

    end
   
    return;
end

function spritzWord = createSpritzWord(word) 
    wordLength = length(word);
    
    spritzWord.word = word;
  
    switch wordLength
        case 1 
            pivotLetter = 1; % first
        case 2 
            pivotLetter = 2; % second
        case 3 
            pivotLetter = 2; % second
        case 4 
            pivotLetter = 2; % second
        case 5 
            pivotLetter = 2; % second
        case 6 
            pivotLetter = 3; % third
        case 7 
            pivotLetter = 3; % third
        case 8 
            pivotLetter = 3; % third
        case 9 
            pivotLetter = 3; % third
        case 10 
            pivotLetter = 4; % fourth
        case 11 
            pivotLetter = 4; % fourth
        case 12 
            pivotLetter = 4; % fourth    
        case 13 
            pivotLetter = 4; % fourth
        otherwise 
            pivotLetter = 5; % fifth
    end
    spritzWord.opt = pivotLetter;
end

function charDimensions = getCharDimensions(window) 
    bbox =  Screen('TextBounds',window, 'o');
    charDimensions = [RectWidth(bbox), RectHeight(bbox)];
	return 
end