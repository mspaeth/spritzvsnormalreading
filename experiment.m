

% Clear the workspace
close all;
clear all;
sca;

% read experiment introduction
introductionFileID = fopen('ExperimentEinfuehrung.csv','r','n','UTF-8');
introductionSet = textscan(introductionFileID,'%s %s% %s %s %s %s %s','Delimiter',';');

% read reading Speed test sentences
readingSpeedFileID = fopen('SpritzLesegeschwindigkeit.csv','r','n','UTF-8');
readingSpeedSet = textscan(readingSpeedFileID,'%s','Delimiter',';');

% read stimuli
stimuliFileID = fopen('StimuliSaetze.csv','r','n','UTF-8');
stimmuliSet = textscan(stimuliFileID,'%s %s %s %s','Delimiter',';');

% Add the current path
addpath(pwd);

% Setup PTB with some default 
PsychDefaultSetup(2);

% Synctest doesn't pass in most develop maschines -> skip it for now
Screen('Preference', 'SkipSyncTests', 1);

% Set the screen number to the external secondary monitor if there is one
% connected
screenNumber = max(Screen('Screens'));

% Define black, white and grey
white = WhiteIndex(screenNumber);
grey = white / 2;
black = BlackIndex(screenNumber);

% Open the screen
[window, windowRect] = PsychImaging('OpenWindow', screenNumber, grey, [], 32, 2);

% set the font to the one Spritz use and text Size (install font in directory first!)
Screen('TextFont', window, 'NN Medien CY Medium');
Screen('TextSize', window, 20);

% Flip to clear
Screen('Flip', window);


% Query the maximum priority level
topPriorityLevel = MaxPriority(window);

% Get the centre coordinate of the window
[xCenter, yCenter] = RectCenter(windowRect);

% Set the blend funciton for the screen
Screen('BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

present = presentation;

testText = 'Marlen|5|0|2|0%m�chte|0|0|2|7%f�r|0|30|1|14%den|0|30|1|18%Geburtstag|0|60|3|22%ihrer|0|0|1|33%Mutter|0|60|2|39%einen|0|0|1|46%leckeren|0|30|2|52%Kuchen|0|60|2|61%backen.|0|110|2|68%|0|0|0|68%Sie|1|30|1|76%sucht|0|0|1|80%verzweifelt|0|30|3|86%nach|0|60|1|98%dem|0|30|1|103%;Zucker;Schl�ssel;Geburtstag;Fahrstuhl;Feier'
testText = 'Marlen|5|0|2|0%m�chte|0|0|2|7%f�r|0|30|1|14%den|0|30|1|18%Geburtstag|0|60|3|22%ihrer|0|0|1|33%Mutter|0|60|2|39%einen|0|0|1|46%leckeren|0|30|2|52%Kuchen|0|60|2|61%backen.|0|110|2|68%|0|0|0|68%Sie|1|30|1|76%sucht|0|0|1|80%verzweifelt|0|30|3|86%nach|0|60|1|98%dem|0|30|1|103%'

present.spritzify(window, xCenter, yCenter, testText, 400);

present.showFixationSpritzbox(window, xCenter, yCenter, .5)
present.presentTarget(window, xCenter, yCenter, 'test', .5)

present.showFixationSpritzbox(window, xCenter, yCenter, .5)
present.presentTarget(window, xCenter, yCenter, 'test2', .5)

Show Experiment Introduction
for introductionTextN = 1:length(introductionSet{1,1})
    
    if(introductionSet{1,4}(introductionTextN) == '') 
        % show normal text
        present.drawContinuousText(window, xCenter, yCenter, char(introductionSet{1,1}(introductionTextN)))
        Screen('Flip', window);
        KbStrokeWait;
    elseif (introductionSet{1,3}(introductionTextN) == 'corsshair')
        present.showCrosshairReact(window, xCenter, yCenter)
    else (introductionSet{1,3}(introductionTextN) == 0) 
        present.spritzify(window, xCenter, yCenter, char(introductionSet{1,1}(introductionTextN)), introductionSet{1,2}(introductionTextN))
    end
end

% determine spritz reading speed
readingSpeed = 250; 
speedStepSize = 50;
% 
% for speedSentenceN = 1:length(readingSpeedSet{1,1})
%     present.spritzify(window, xCenter, yCenter, char(readingSpeedSet{1,1}(speedSentenceN)), readingSpeed);
%     toSlow = present.askQuestion(window, xCenter, yCenter, 'Hat sich das Lesen ''Robotisch'',\n ''abgehackt'' oder wie ein Stroboskop angef�hlt?');
%     WaitSecs(1)
%     if(toSlow == 1)
%         readingSpeed = readingSpeed + speedStepSize
%     else 
%         toFast = present.askQuestion(window, xCenter, yCenter, 'Hat dich das Lesen gestresst?');
%         if(toFast == 1)
%             readingSpeed = readingSpeed - speedStepSize
%         end
%     end
% end 

% present.drawContinuousText(window, xCenter, yCenter, ['Ihre Spritzgeschwindigkeit ist \n' readingSpeed])
%         Screen('Flip', window);
% KbStrokeWait;
     

for stimmuliSetN = 1:length(stimmuliSet{1,1})
    present.spritzify(window, xCenter, yCenter, char(stimmuliSet{1,1}(stimmuliSetN)), readingSpeed);
    present.showFixationSpritzboxReact(window, xCenter, yCenter);
    present.showFixationSpritzbox(window, xCenter, yCenter, .5)
    present.presentTarget(window, xCenter, yCenter,  char(stimmuliSet{1,2}(stimmuliSetN)), .5)
    
    toSlow = present.askQuestion(window, xCenter, yCenter, 'War das folgende Wort enthalten?');
    present.showFixationSpritzbox(window, xCenter, yCenter, .5)
    present.presentTarget(window, xCenter, yCenter,  char(stimmuliSet{1,4}(stimmuliSetN)), .5)
    
end 

% Experiment f�r normales Lesen implementieren

% TODO Satz 1 und Satz 2 bis zum Target Zeigen Timestamp speichern 
% TODO auf Tastendruck warten und zweiten Timestamp speichern


% Experiment f�r Spritz lesen implementieren
% TODO Spritz Maske f�r 500 ms einblenden
% TODO Satz 1 und Satz 2 bis zum Targez Zeigen Timestamp speichern 
% TODO Marker bei jedem Wort?
% TODO Fixazionskreuz anzeigen
% TODO auf Tastendruck warten und zweiten Timestamp speichern

% Zielreizdarbietung

% TODO Fixazionskreuz f�r 500 ms anzeigen
% TODO Zielwort anzeigen und Marker f�r Target/Nontarget schicken
% TODO Verst�ndnisfrage aufrufen

% Verst�ndnisfrage implementieren
% TODO Text anzeigen: "War folgendes Wort in den dargebotenen Zwei S�tzen
% enthalten: "
% Spritz box anzeigen, 500 ms warten wort das enthalten war oder nicht in
% die Box zeichnen ud Marker senden


