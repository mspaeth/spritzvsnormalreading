
function initialize()

    % Clear the workspace
    close all;
    clear all;
    sca;
    
    global window windowRect topPriorityLevel xCenter yCenter present mark
    
    % Add the current path
    addpath(pwd);

    % Setup PTB with some default 
    PsychDefaultSetup(2);

    % Synctest doesn't pass in most develop maschines -> skip it for now
    Screen('Preference', 'SkipSyncTests', 1);

    Screen('Preference', 'TextRenderer', 1);
    
    % Set the screen number to the external secondary monitor if there is one
    % connected
    screenNumber = max(Screen('Screens'));
    
    % Define black, white and grey
    white = WhiteIndex(screenNumber);
    grey = white / 2;
    black = BlackIndex(screenNumber);
    
    [window, windowRect] = PsychImaging('OpenWindow', screenNumber, grey, [], 32, 2);

    % set the font to the one Spritz use and text Size (install font in directory first!)
    Screen('TextFont', window, 'NN Medien CY Medium');
    
    Screen('TextSize', window, 20);

    % Flip to clear
    Screen('Flip', window);

    % Open the screen

    % Query the maximum priority level
    topPriorityLevel = MaxPriority(window);

    % Get the centre coordinate of the window
    [xCenter, yCenter] = RectCenter(windowRect);

    % Set the blend funciton for the screen
    Screen('BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

    present = presentation;
    mark = marker;
    mark.initialize('D010', 1);

    initExperimentData();
    
    runExperiment();
end 

function initExperimentData()
    feature('DefaultCharacterSet','ISO-8859-1');
    global pauseAfterReadingTrail intertrailPause pauseAfterSpritzTrail nReadingStimuli nSpritzStimuli WPM introductionSet readingSpeedSet stimmuliSet readingStimuliIndices spritzStimuliIndices showQuestionDuration showTargetDuration showFixationSpritzbox spritzStimuliIndicessI readingStimuliIndicesI
    % read experiment introduction
    introductionFileID = fopen('ExperimentEinfuehrung.csv','r','n','ISO-8859-1');
    introductionSet = textscan(introductionFileID,'%s %s %d','Delimiter',';');
    fclose(introductionFileID);
    
    % read reading Speed test sentences
    readingSpeedFileID = fopen('SpritzLesegeschwindigkeit.csv','r','n','ISO-8859-1');
    readingSpeedSet = textscan(readingSpeedFileID,'%s %s','Delimiter',';');
    fclose(readingSpeedFileID);

    stimuliFileID = fopen('StimuliSaetze.csv','r','n','ISO-8859-1');
    stimmuliSet = textscan(stimuliFileID,'%s %s %s %s %s %s %s','Delimiter',';');
    fclose(stimuliFileID);
    
    % stimmuliSet{1,1}{n} -> n. spritz parsed sentence
    % stimmuliSet{1,2}{n} -> n. normal sentence
    % stimmuliSet{1,3}{n} -> n. match stimuli
    % stimmuliSet{1,4}{n} -> n. missmatch stimuli
    % stimmuliSet{1,5}{n} -> n. question word 1 
    % stimmuliSet{1,6}{n} -> n. question word 2 
    % stimmuliSet{1,7}{n} -> n. question word 3 

    % randomize target stimuli (stimmuliSet{1,8})
    stimmuliSet = [stimmuliSet randi(2,length(stimmuliSet{1,1}),1)]
    % stimmuliSet{1,8}{n} -> randomized stimulus
    
    % randomize question stimuli (stimmuliSet{1,9})
    stimmuliSet = [stimmuliSet (randi(2,length(stimmuliSet{1,1}),1)+1)]
    % stimmuliSet{1,9}{n} -> randomized question word

    % read stimuli
    % every 4th sentence a question
    stimmuliSet = [stimmuliSet randi(4,length(stimmuliSet{1,1}),1)]
    % stimmuliSet{1,10}{n} -> randomized question indicator(=1 ask question)
   

    randomizedIndices  = randperm(length(stimmuliSet{1,1}))
    % n reading stimuli (including 4 for the sample trails!)
    nReadingStimuli = 125;
    readingStimuliIndices = randomizedIndices(1:nReadingStimuli)
    readingStimuliIndicesI = 1;
    
    pauseAfterReadingTrail = 15;
    
    % n spritz stimuli (including 4 for the sample trails!)
    nSpritzStimuli = 125;
    spritzStimuliIndices = randomizedIndices((nReadingStimuli+1):(nSpritzStimuli+nReadingStimuli+1))
    spritzStimuliIndicessI = 1;
    
    pauseAfterSpritzTrail = 15;
    
    % how long shoud we show the target stimulus?
    showTargetDuration = 1;
    showQuestionDuration = 2;
    showFixationSpritzbox = 1;
    intertrailPause = 1;
    WPM = 450;
end 

function runExperiment()
    global present window xCenter yCenter mark
    
    runIntroduction();
    if(randi(2) == 2)
        
        present.drawContinuousText(window, xCenter, yCenter,  'Wir beginnen mit dem Spritztext \n Bitte dr�cken Sie eine der Pfeiltasten um zu beginnen.');
        Screen('Flip', window);
        KbStrokeWait;
        
        % 1: Spritzblock started
        mark.sendMarker(1);
        runSpritzExperiment();
        
        present.drawContinuousText(window, xCenter, yCenter,  'Als n�chstes beginnt der Flie�text \n Bitte dr�cken Sie eine der Pfeiltasten um fortzufahren.');
        Screen('Flip', window);
        KbStrokeWait;
        
        % 2: Flie�textblock started
        mark.sendMarker(2);
        runReadingExperiment();
    else
        present.drawContinuousText(window, xCenter, yCenter,  'Wir beginnen mit dem Flie�text \n Bitte dr�cken Sie eine der Pfeiltasten um zu beginnen.');
        Screen('Flip', window);
        KbStrokeWait;
        
        % 2: Flie�textblock started
        mark.sendMarker(2);
        runReadingExperiment();
        
        present.drawContinuousText(window, xCenter, yCenter,  'Als n�chstes beginnt der Spritztext \n Bitte dr�cken Sie eine der Pfeiltasten um fortzufahren.');
        Screen('Flip', window);
        KbStrokeWait;
        
        % 1: Spritzblock started
        mark.sendMarker(1);
        runSpritzExperiment();
    end
    
    runEnd();
end

function runSpritzExperiment
    global present nSpritzStimuli window xCenter yCenter spritzStimuliIndicessI  pauseAfterSpritzTrail spritzStimuliIndices mark
    currentTrail = 1;
    
    for trail = spritzStimuliIndicessI:nSpritzStimuli
        
        spritzStimulusIndex = spritzStimuliIndices(spritzStimuliIndicessI);
        runSpritzTrail(spritzStimulusIndex)
        spritzStimuliIndicessI = spritzStimuliIndicessI +1;
        
        if(mod(currentTrail,pauseAfterSpritzTrail) == 0)
            
            % 21: Pause
            mark.sendMarker(21);
            
            screenRect = present.drawContinuousText(window, xCenter, yCenter, 'Sie k�nnen nun eine kurze Pause machen. Bitte dr�cken Sie eine der Pfeiltasten um das Experiment fortzusetzen.')
            
            Screen('TextSize', window, 100);
            DrawFormattedText(window, 'PAUSE!', 'center', screenRect(2)+10, [0 0 1], 30, 0, 0, 2);
            Screen('TextSize', window, 20);
            Screen('Flip', window);
            
            KbStrokeWait;
            % 22: End Pause
            mark.sendMarker(22);
            
        end 
        
        currentTrail = currentTrail + 1;
    end
end

function runReadingExperiment
    global present window xCenter yCenter nReadingStimuli readingStimuliIndicesI pauseAfterReadingTrail readingStimuliIndices mark 
    currentTrail = 1;
    
    for trail = readingStimuliIndicesI:nReadingStimuli
        
        
        readingStimulusIndex = readingStimuliIndices(readingStimuliIndicesI);
        runReadingTrail(readingStimulusIndex)    
        readingStimuliIndicesI = readingStimuliIndicesI +1;
        if(mod(currentTrail,pauseAfterReadingTrail) == 0)
            
            % 21: Pause
            mark.sendMarker(21);
            
            screenRect = present.drawContinuousText(window, xCenter, yCenter, 'Sie k�nnen nun eine kurze Pause machen. Bitte dr�cken Sie eine der Pfeiltasten um das Experiment fortzusetzen.')
            
            Screen('TextSize', window, 100);
            DrawFormattedText(window, 'PAUSE!', 'center', screenRect(2)+10, [0 0 1], 30, 0, 0, 2);
            Screen('TextSize', window, 20);
            Screen('Flip', window);
            KbStrokeWait;
            
            % 22: End Pause
            mark.sendMarker(22);
        end 
        
        currentTrail = currentTrail + 1;
    end
end

function runEnd()  
    global window  xCenter yCenter present data WPM
    screenRect = present.drawContinuousText(window, xCenter, yCenter, 'Puh! Geschafft ;-)! Vielen Dank f�r die Teilnahme an diesem Experiment.')
    
    
    save('vp1.mat', ['data']);
            
    Screen('TextSize', window, 90);
    DrawFormattedText(window, 'DANKE!', 'center', screenRect(2)+10, [0 0 1], 30, 0, 0, 2);
    Screen('TextSize', window, 20);
    Screen('Flip', window);
end
function runIntroduction() 
    global WPM present introductionSet window xCenter yCenter  spritzStimuliIndicessI readingSpeedSet stimmuliSet readingStimuliIndices spritzStimuliIndices readingStimuliIndicesI
    
            runSpritzTrail(1, true)
            
            runReadingTrail(3, true)
            runSpritzTrail(5, true)
            
            runReadingTrail(2, true)
            
    for introductionTextN = 1:length(introductionSet{1,1})

        if(introductionSet{1,3}(introductionTextN) == 0) 
            % 0 for show normal text
            present.drawContinuousText(window, xCenter, yCenter, char(introductionSet{1,2}(introductionTextN)))
            Screen('Flip', window);
            KbStrokeWait;
        elseif (introductionSet{1,3}(introductionTextN) == 6)
            % 6 for spritz speedanalyzer   
            WPM = runSpritzSpeedAnalyzer();
        elseif (introductionSet{1,3}(introductionTextN) == 1)
            % 1 for simple crosshair
            present.showCrosshairReact(window, xCenter, yCenter);
        elseif (introductionSet{1,3}(introductionTextN) == 2)
            % 2 for spritz trail without question
            spritzStimulusIndex = spritzStimuliIndices(spritzStimuliIndicessI);
            runSpritzTrail(spritzStimulusIndex, false)
            spritzStimuliIndicessI = spritzStimuliIndicessI + 1;
        elseif (introductionSet{1,3}(introductionTextN) == 3)
            % 3 for spritz trail with question
            spritzStimulusIndex = spritzStimuliIndices(spritzStimuliIndicessI);
            runSpritzTrail(spritzStimulusIndex, true)
            spritzStimuliIndicessI = spritzStimuliIndicessI +1;
        elseif (introductionSet{1,3}(introductionTextN) == 4)
            % 4 for normal reading trail with question
            readingStimulusIndex = readingStimuliIndices(readingStimuliIndicesI);
            runReadingTrail(readingStimulusIndex, false)
            readingStimuliIndicesI = readingStimuliIndicesI +1;
        elseif (introductionSet{1,3}(introductionTextN) == 5)
            % 5 for normal reading trail with question
            readingStimulusIndex = readingStimuliIndices(readingStimuliIndicesI);
            runReadingTrail(readingStimulusIndex, true)
            readingStimuliIndicesI = readingStimuliIndicesI +1;
        else 
            % every thing else for spritz presentation (the number represents
            % the words per minute at the same time)
            fixWpm = introductionSet{1,3}(introductionTextN);
            present.spritzify(window, xCenter, yCenter, char(introductionSet{1,1}(introductionTextN)), fixWpm);
        end
    end
end


function runSpritzTrail(stimulusIndex, ask)
    global WPM present window xCenter yCenter stimmuliSet showTargetDuration showFixationSpritzbox intertrailPause mark data
    if(nargin == 1)
        ask = ( stimmuliSet{1,10}(stimulusIndex) == 1 );
    end
    
    save('vp1.mat', ['data']);
    
    % send Marker for the sentence Id + 100
    % mark.sendMarker(stimulusIndex+100);
    
    % TODO write marker (text shown)
    present.spritzify(window, xCenter, yCenter,  stimmuliSet{1,1}{stimulusIndex},WPM);
    present.showCrosshairReact(window, xCenter, yCenter);
    present.showFixationSpritzbox(window, xCenter, yCenter,showFixationSpritzbox);
    
    % the random column contains values 1 or 2... add the previous column
    % indeces (2) to get the correct column
    stimulusColum = stimmuliSet{1,8}(stimulusIndex) + 2; 
   
    stimmuliSet{1,stimulusColum}{stimulusIndex}
        
    % TODO write marker (target/nonTarget depending on stimmuliSet{1,2})
    % calculate stimulus marker  
    % 10: Anzeige des Konformen Targets
    % 11: Anzeige des Nonkonformen Targets
    stimulusMarker = stimmuliSet{1,8}(stimulusIndex) + 9;
    mark.sendMarker(stimulusMarker);
    
    present.drawSingleSpritzWord(window, xCenter, yCenter, stimmuliSet{1,stimulusColum}{stimulusIndex});
    WaitSecs(showTargetDuration);
    
    
    % 12: Ende der Anzeige des Targets
    mark.sendMarker(12);
    
    % pause between trails
    present.drawContinuousText(window, xCenter, yCenter, ' ');
    Screen('Flip', window);
    WaitSecs(intertrailPause);
    
    if(ask) 
        askQuestion(stimulusIndex)
    end
end


function runReadingTrail(stimulusIndex, ask)
    global present intertrailPause window xCenter yCenter stimmuliSet showTargetDuration showFixationSpritzbox mark data
    if(nargin == 1)
        ask = ( stimmuliSet{1,10}(stimulusIndex) == 1 );
    end
    
    save('vp1.mat', ['data']);
    
    % send Marker for the sentence Id + 100
    % mark.sendMarker(stimulusIndex+100);
    
    stimmuliSet{1,2}{stimulusIndex}
   
    % 3: Flie�textsatz angezeigt (fr�hster Zeitpunkt f�r das Beginnen des Lesens)
    mark.sendMarker(3);
    
    present.drawContinuousText(window, xCenter, yCenter,  stimmuliSet{1,2}{stimulusIndex});
    Screen('Flip', window);
    KbStrokeWait;
    
    
    % 4: Flie�textsatz fertig gelesen (nach Tastendruck)
    mark.sendMarker(4);
    present.showFixationSpritzbox(window, xCenter, yCenter,showFixationSpritzbox);
    
    % the random column contains values 1 or 2... add the previous column
    % indeces (2) to get the correct column
    stimulusColum = stimmuliSet{1,8}(stimulusIndex) + 2; 
   
    stimmuliSet{1,stimulusColum}{stimulusIndex}
    
    % calculate stimulus marker  
    % 10: Anzeige des Konformen Targets
    % 11: Anzeige des Nonkonformen Targets
    stimulusMarker = stimmuliSet{1,8}(stimulusIndex) + 9;
    mark.sendMarker(stimulusMarker);
    
    present.drawSingleSpritzWord(window, xCenter, yCenter, stimmuliSet{1,stimulusColum}{stimulusIndex});
    WaitSecs(showTargetDuration);
    
    % 12: Ende der Anzeige des Targets
    mark.sendMarker(12);
    
    % pause between trails
    present.drawContinuousText(window, xCenter, yCenter, ' ');
    Screen('Flip', window);
    WaitSecs(intertrailPause);
    
    if(ask) 
        askQuestion(stimulusIndex)
    end 
end

function wpm = runSpritzSpeedAnalyzer() 
    global  present window xCenter yCenter readingSpeedSet
    wpm = 250;
    increment = 100;
    
    readingSpeedSetI = 1;
    
    TOFAST =  KbName('DownArrow');
    GOOD =  KbName('RightArrow');
    TOSLOW = KbName('UpArrow');
    
    lastChoice = TOSLOW;
    secondLastChoice = GOOD;
    
    while(not(lastChoice == GOOD && secondLastChoice == GOOD)) 
        present.spritzify(window, xCenter, yCenter, char(readingSpeedSet{1,1}(readingSpeedSetI)), wpm);
        readingSpeedSetI = readingSpeedSetI +1;
        
        secondLastChoice = lastChoice; 
        lastChoice = 0;
        while lastChoice == 0
        
            screenRect = present.drawScreenRect(window, xCenter, yCenter);
            DrawFormattedText(window, 'Erschien der dargestellte Text abgehackt oder "Roboterartig"?\nDann dr�cken Sie bitte den Pfeil nach oben um die Geschwindigkeit etwas zu erh�hen.\nWar das Lesen stressig oder ging es zu schnell, dr�cken Sie Bitte den Pfeil nach unten.\nWar das Lesen angenehm, dann dr�cken sie den Pfeil nach rechts.' , screenRect(1), screenRect(2), 0, 30, 0, 0, 2);
            Screen('Flip', window);

            % Check the keyboard. The person should press the
            [secs, keyCode, deltaSecs] = KbWait([], 2);
            if keyCode(TOFAST)
                lastChoice = TOFAST;
            elseif keyCode(GOOD)
                lastChoice = GOOD;
            elseif keyCode(TOSLOW)
                lastChoice = TOSLOW;
            end
        end
        
        % if the user choose slower after speeding up or if he choose
        % faster after slowing down we decrease the increment
        if(lastChoice ~= GOOD && secondLastChoice ~= GOOD && lastChoice ~= secondLastChoice)
            if(increment == 100)
                increment = 50;
            elseif (increment == 50)
                increment = 25;
            elseif (increment == 25)
                increment = 10;
            elseif (increment == 10)
                increment = 5;
            end
        end
        
        lastChoice
        if(lastChoice == TOSLOW)
            wpm = wpm + increment;
        elseif (lastChoice == TOFAST)
            wpm = wpm - increment;
        end
    end
    
    
    present.drawContinuousText(window, xCenter, yCenter,  ['Vielen Dank! \nIhre Spritz Lesegeschwindigkeit betr�gt \n' int2str(wpm) ' W�rter pro Minute']);
    Screen('Flip', window);
    KbStrokeWait;
    
    
end

function askQuestion(stimulusIndex) 
     
    global present showQuestionDuration stimmuliSet window xCenter yCenter intertrailPause showFixationSpritzbox mark
    % 13: Anzeige des Fragezeichens
    mark.sendMarker(13);
    present.drawQuestionmark(window, xCenter, yCenter, 1.5);
    % 14: Anzeige der Frage
    mark.sendMarker(14)
    present.drawContinuousText(window, xCenter, yCenter, 'Passt das folgende Wort zum vorherigen Satz?');
    Screen('Flip', window);
    
    KbStrokeWait;
    %WaitSecs(showQuestionDuration);
    
    % 15: Einblenden der Fixations spritzbox f�r die Frage
    mark.sendMarker(15);
    % show fixation Spritzbox
    present.showFixationSpritzbox(window, xCenter, yCenter,showFixationSpritzbox);
    % the random column contains values 1 or 2 or 3... add the previous column
    % indeces (4) to get the correct column
    % TODO write marker (q1 q2 q3 depending onstimmuliSet{1,2}(n)
    stimulusColum = stimmuliSet{1,9}(stimulusIndex) + 4; 
    
    % calculate stimulus marker  
    % 16: Einblenden des Fragereizes - 1 Wie nennen wir dieses target?
    % 17: Einblenden des Fragereizes - 2 Wie nennen wir dieses target?
    % 18: Einblenden des Fragereizes - 3 Wie nennen wir dieses target?
    stimulusMarker = stimmuliSet{1,9}(stimulusIndex) + 15;
    
    correctAnswer =  stimmuliSet{1,9}(stimulusIndex);
    
    mark.sendMarker(stimulusMarker);
    
    response = present.drawSpritzWordReact(window, xCenter, yCenter, stimmuliSet{1,stimulusColum}{stimulusIndex})
   
    % 26: Reaktion auf den Fragereiz - 1 mit Nein
    % 36: Reaktion auf den Fragereiz - 1 mit Ja
    % 
    % 27: Reaktion auf den Fragereiz - 2 mit Nein
    % 37: Reaktion auf den Fragereiz - 2 mit Ja
    % 
    % 28: Reaktion auf den Fragereiz - 3 mit Nein
    % 38: Reaktion auf den Fragereiz - 3 mit Ja
    % response for yes = 1 for no = 0
    answerMarker = stimulusMarker + 10 + (10 * response);
    mark.sendMarker(answerMarker);
    
    screenRect = present.drawContinuousText(window, xCenter, yCenter, ' ')
            
    Screen('TextSize', window, 70);
    if((response+2) == correctAnswer)
        DrawFormattedText(window, 'Richtig!', 'center', 'center', [0 1 0], 30, 0, 0, 2);
    else
        DrawFormattedText(window, 'Falsch!', 'center', 'center', [1 0 0], 30, 0, 0, 2);
    end
    
    Screen('TextSize', window, 20);
    Screen('Flip', window);
    
    % pause between trails
    WaitSecs(intertrailPause);
    
end
