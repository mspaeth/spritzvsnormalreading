function funs = marker
    funs.initialize = @initialize;
    funs.sendMarker = @sendMarker;
end

function initialize(parallelAddress, simulate)
    global ioObj address impWidth doSimulate data
    if(simulate)
        doSimulate = true;
        return
    end
    data = []
    
    %% Setup Parallelport
    ioObj = io64; %create object
    status = io64(ioObj); %initialize the inpoutx64 system driver; if status = 0, you are now ready to write and read to a hardware port
    address = hex2dec(parallelAddress); %('E010'); %set the hardware address (look up in windows device manager)
    impWidth = 0.0005; %s
end

function sendMarker(marker)
      global data
      t = GetSecs;    %check current time
      curMarker.time = t;
      curMarker.marker = marker;
      data = [data, curMarker];

      global ioObj address impWidth doSimulate
      if(doSimulate)
          'marker simulation'
          marker
          return
      end
      io64(ioObj,address,marker)    %set parport pins to high
      WaitSecs('UntilTime',t+impWidth);    %wait until t + impWidth
      io64(ioObj,address,0)    %set parport pins to low again
end
